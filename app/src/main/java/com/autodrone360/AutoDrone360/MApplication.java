package com.autodrone360.AutoDrone360;

import android.app.Application;
import android.content.Context;

import com.secneo.sdk.Helper;

public class MApplication extends Application {

    private AutoDrone360Application application;

    @Override
    protected void attachBaseContext(Context paramContext) {
        super.attachBaseContext(paramContext);
        Helper.install(MApplication.this);
        if (application == null) {
            application = new AutoDrone360Application();
            application.setContext(this);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application.onCreate();
    }

}
